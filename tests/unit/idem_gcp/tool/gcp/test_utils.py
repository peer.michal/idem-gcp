from typing import Dict

import yaml

_SLS_1 = """
idem-gcp-vm:
  gcp.compute.instance.present:
  - name: idem-gcp-vm-2
  - project: project-name
  - description: 'Description'
  - network_interfaces:
    - access_configs:
      - kind: compute#accessConfig
        name: External NAT
        nat_ip: 34.67.190.142
        network_tier: PREMIUM
        type_: ONE_TO_ONE_NAT
      - kind: compute#accessConfig2
        name: External NAT
        nat_ip: 34.67.190.142
        network_tier: PREMIUM
        type_: ONE_TO_ONE_NAT
      fingerprint: GpxokVYsg7M=
      kind: compute#networkInterface
      name: nic0
      network: projects/project-name/global/networks/default
      network_ip: 10.128.15.203
      stack_type: IPV4_ONLY
      subnetwork: projects/project-name/regions/us-central1/subnetworks/default
  - label_fingerprint: 42WmSpB8rSM=
  - fingerprint: 0WBA6Ipp35U=
"""

# changed: label_fingerprint, fingerprint, description
# added: key_revocation_action_type
# removed: project
_SLS_2 = """
idem-gcp-vm:
  gcp.compute.instance.present:
  - name: idem-gcp-vm-2
  - description: 'New Description'
  - network_interfaces:
    - access_configs:
      - kind: compute#accessConfig
        name: External NAT
        nat_ip: 34.67.190.142
        network_tier: PREMIUM
        type_: ONE_TO_ONE_NAT
      - kind: compute#accessConfig2
        name: External NAT
        nat_ip: 34.67.190.142
        network_tier: PREMIUM
        type_: ONE_TO_ONE_NAT
      fingerprint: GpxokVYsg7M=
      kind: compute#networkInterface
      name: nic0
      network: projects/project-name/global/networks/default
      network_ip: 10.128.15.203
      stack_type: IPV4_ONLY
      subnetwork: projects/project-name/regions/us-central1/subnetworks/default
  - label_fingerprint: 52WmSpB8rSM=
  - fingerprint: 1WBA6Ipp35U=
  - key_revocation_action_type: NONE
"""


def test_compare_states(hub):
    s1 = yaml.load(_SLS_1, Loader=yaml.Loader)
    l1 = s1["idem-gcp-vm"]["gcp.compute.instance.present"]
    s1 = {}
    for l0 in l1:
        s1.update(**l0)

    s2 = yaml.load(_SLS_2, Loader=yaml.Loader)
    l2 = s2["idem-gcp-vm"]["gcp.compute.instance.present"]
    s2 = {}
    for l0 in l2:
        s2.update(**l0)

    changes: Dict = hub.tool.gcp.utils.compare_states(s1, s2, "compute.instance")
    assert changes is not None
    assert changes.keys() == {
        "values_changed",
        "dictionary_item_added",
        "relevant_changes",
    }
    assert changes["values_changed"] == {
        "root['description']": {
            "new_value": "New Description",
            "old_value": "Description",
        }
    }
    assert changes["dictionary_item_added"] == {"root['key_revocation_action_type']"}

    # removed property "project" is not in relevant_changes, as well as excluded ones for the resource type
    assert set(changes["relevant_changes"]) == {
        "root['description']",
        "root['key_revocation_action_type']",
    }


# bug scenario in deepdiff - custom_placement_config is added to excluded paths, but still present in changes.affected_paths
def test_compare_states_relevant_changes_affected_paths_bug(hub):
    old_state = {"custom_placement_config": None, "name": "x"}
    new_state = {"name": "y"}
    changes = hub.tool.gcp.utils.compare_states(old_state, new_state, "storage.bucket")
    # affected_path is incorrect
    assert set(changes.affected_paths) == {
        "root['custom_placement_config']",
        "root['name']",
    }
    assert set(changes["relevant_changes"]) == {"root['name']"}


def test_get_plan_state_value_from_deep_diff_path(hub):
    plan_state = {
        "root_prop": "root_prop_value",
        "parent_prop": {"nested_prop": "nested_prop_value"},
        "list_prop": [
            {"item_prop": "item_prop_value"},
            {"item_prop": "item_prop_value_2"},
        ],
    }
    value = hub.tool.gcp.utils.get_plan_state_value_from_deep_diff_path(
        "root['root_prop']", plan_state
    )
    assert value == "root_prop_value"
    value = hub.tool.gcp.utils.get_plan_state_value_from_deep_diff_path(
        "root['parent_prop']['nested_prop']", plan_state
    )
    assert value == "nested_prop_value"
    value = hub.tool.gcp.utils.get_plan_state_value_from_deep_diff_path(
        "root['list_prop'][1]['item_prop']", plan_state
    )
    assert value == "item_prop_value_2"

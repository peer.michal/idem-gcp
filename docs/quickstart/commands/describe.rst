Describe
========

The *Describe* command will show you how your environment is currently
configured. The output describing your environment can also be used to
configure and update your environment.

.. include:: ./_includes/auth-note.rst

Examples
++++++++

Networks
--------

List and Describe your networks:

.. code-block:: bash

    idem describe gcp.compute.network

Output:

.. code-block:: sls

    projects/<project_name>/global/networks/<network_name>
      gcp.compute.network.present:
      - name: <network_name>
      - auto_create_subnetworks: false
      - peerings:
        - auto_create_routes: true
          exchange_subnet_routes: true
          export_custom_routes: false
          export_subnet_routes_with_public_ip: true
          import_custom_routes: false
          import_subnet_routes_with_public_ip: false
          name: <peering_name>
          network: https://www.googleapis.com/compute/v1/projects/<project_name>/global/networks/<network_name>
          stack_type: IPV4_ONLY
          state: ACTIVE
          state_details: '[2023-02-16T03:36:26.805-08:00]: Connected.'
      - routing_config:
          routing_mode: REGIONAL
      - network_firewall_policy_enforcement_order: AFTER_CLASSIC_FIREWALL
      - resource_id: projects/<project_name>/global/networks/<network_name>

The above example shows info about each network. In the next page discussing the
`Idem State </quickstart/commands/state>`_ we'll show how you can use the
output of the `Describe` command to configure the state of existing resources.

Instances
---------

List and Describe your GCP Instances

.. code-block:: bash

    idem describe gcp.compute.instance

Output:

.. code-block:: sls

    projects/<project_name>/zones/us-central1-a/instances/<instance_name>:
      gcp.compute.instance.present:
      - name: <instance_name>
      - tags:
          fingerprint: 42WmSpB8rSM=
      - machine_type: https://www.googleapis.com/compute/v1/projects/<project_name>/zones/us-central1-a/machineTypes/e2-micro
      - status: STOPPING
      - zone: us-central1-a
      - network_interfaces:
        - fingerprint: 5nEUL6IlOr0=
          kind: compute#networkInterface
          name: nic0
          network: https://www.googleapis.com/compute/v1/projects/<project_name>/global/networks/default
          network_ip: <ip>
          stack_type: IPV4_ONLY
          subnetwork: https://www.googleapis.com/compute/v1/projects/<project_name>/regions/us-central1/subnetworks/default
      - disks:
        - auto_delete: false
          boot: true
          device_name: persistent-disk-0
          disk_size_gb: '1'
          index: 0
          interface: SCSI
          kind: compute#attachedDisk
          mode: READ_WRITE
          source: https://www.googleapis.com/compute/v1/projects/<project_name>/zones/us-central1-a/disks/<disk_name>
          type: PERSISTENT
      - metadata:
          fingerprint: zrn-PNxgE6M=
          kind: compute#metadata
      - scheduling:
          automatic_restart: true
          on_host_maintenance: MIGRATE
          preemptible: false
          provisioning_model: STANDARD
      - label_fingerprint: 42WmSpB8rSM=
      - deletion_protection: false
      - fingerprint: lF5m-IjAhOc=
      - resource_id: projects/<project_name>/zones/us-central1-a/instances/<instance_name>

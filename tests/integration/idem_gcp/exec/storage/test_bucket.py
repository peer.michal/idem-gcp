from typing import Any
from typing import Dict
from typing import List

import pytest

from tests.utils import generate_unique_name

RESOURCE_TYPE_BUCKET = "storage.bucket"

PRESENT_STATE_BUCKET = {"name": "test-bucket"}


@pytest.fixture(scope="module")
def gcp_buckets(hub, idem_cli) -> List[Dict[str, Any]]:
    bucket_params = [
        {"name": generate_unique_name("idem-test-bucket-1")},
        {"name": generate_unique_name("idem-test-bucket-2")},
    ]

    buckets = []
    for bucket in bucket_params:
        bucket_present_ret = hub.tool.utils.call_present_from_properties(
            idem_cli, RESOURCE_TYPE_BUCKET, {**PRESENT_STATE_BUCKET, **bucket}
        )
        assert bucket_present_ret["result"]
        created_bucket = bucket_present_ret["new_state"]
        assert created_bucket
        assert created_bucket.get("name")
        assert created_bucket.get("resource_id")
        buckets.append(created_bucket)

    yield buckets

    for bucket in buckets:
        bucket_absent_ret = hub.tool.utils.call_absent(
            idem_cli,
            RESOURCE_TYPE_BUCKET,
            bucket.get("name"),
            bucket.get("resource_id"),
        )
        assert bucket_absent_ret["result"]
        assert not bucket_absent_ret.get("new_state")


@pytest.fixture(scope="module")
def retention_policy_bucket(hub, idem_cli, gcp_buckets) -> Dict[str, Any]:
    bucket = gcp_buckets[0]
    # configure a retention policy to lock
    present_props = {
        "name": bucket.get("name"),
        "resource_id": bucket.get("resource_id"),
        "retention_policy": {"retention_period": 20},
    }
    bucket_present_ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_BUCKET, present_props
    )
    assert bucket_present_ret["result"]
    updated_bucket = bucket_present_ret["new_state"]
    assert updated_bucket.get("retention_policy", {}).get("retention_period") == "20"
    assert not updated_bucket.get("retention_policy", {}).get("is_locked")
    return updated_bucket


@pytest.mark.asyncio
async def test_get_missing_bucket_name(hub, ctx):
    ret = await hub.exec.gcp.storage.bucket.get(
        ctx,
        name=None,
    )
    assert not ret["result"], not ret["ret"]
    assert ret["comment"]
    assert 'Missing required parameter "bucket"' in ret["comment"]


@pytest.mark.asyncio
async def test_get_non_existent_bucket_name(hub, ctx):
    ret = await hub.exec.gcp.storage.bucket.get(
        ctx,
        name="invalidbucketname",
    )
    assert ret["result"]
    assert ret["ret"] is None
    assert ret["comment"] == []


@pytest.mark.asyncio
async def test_get_bucket(hub, ctx, gcp_buckets):
    bucket = gcp_buckets[0]
    ret = await hub.exec.gcp.storage.bucket.get(
        ctx,
        name=bucket.get("name"),
    )
    assert ret["result"], ret["ret"]
    assert ret["comment"] == []
    assert bucket == ret["ret"]
    assert "metageneration" in bucket


@pytest.mark.asyncio
async def test_get_bucket_full_projection(hub, ctx, gcp_buckets):
    bucket = gcp_buckets[0]
    ret = await hub.exec.gcp.storage.bucket.get(
        ctx,
        name=bucket.get("name"),
        projection="full",
    )
    assert ret["result"], ret["ret"]
    assert ret["comment"] == []
    assert ret["ret"]["acl"]


@pytest.mark.asyncio
async def test_list_buckets(hub, ctx, gcp_buckets):
    ret = await hub.exec.gcp.storage.bucket.list(
        ctx,
    )

    assert ret["ret"], ret["result"]
    assert len(ret["ret"]) >= 2
    assert gcp_buckets[0] in ret["ret"]
    assert gcp_buckets[1] in ret["ret"]
    assert "metageneration" in ret["ret"][0]


@pytest.mark.asyncio
async def test_list_bucket_filter_by_prefix(hub, ctx, gcp_buckets):
    bucket = gcp_buckets[0]

    ret = await hub.exec.gcp.storage.bucket.list(
        ctx,
        prefix=bucket["name"],
    )

    assert ret["ret"], ret["result"]
    assert len(ret["ret"]) >= 1
    assert bucket in ret["ret"]


@pytest.mark.asyncio
async def test_lock_retention_policy(hub, ctx, idem_cli, retention_policy_bucket):
    ret = await hub.exec.gcp.storage.bucket.lock_retention_policy(
        ctx,
        name=retention_policy_bucket.get("name"),
        if_metageneration_match=retention_policy_bucket.get("metageneration"),
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    assert ret["comment"] == []
    assert ret["ret"].get("retention_policy", {}).get("is_locked") is True
    assert ret["ret"].get("retention_policy", {}).get("retention_period") == "20"

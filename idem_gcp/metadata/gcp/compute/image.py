"""Metadata module for managing Images."""

PATH = "projects/{project}/global/images/{image}"

NATIVE_RESOURCE_TYPE = "compute.images"

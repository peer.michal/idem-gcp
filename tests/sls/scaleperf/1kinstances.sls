{% set project_id = 'scale-perf-proj' %}

scaleperf-instance:
  gcp.compute.instance.present:
  - names:
    {% for i in range(1000) %}
            test-{{ loop.index }}: {{loop.index}}
    {% endfor %}
  - project: {{project_id}}
  - machine_type: https://www.googleapis.com/compute/v1/projects/{{project_id}}/zones/us-central1-a/machineTypes/g1-small
  - zone: us-central1-a
  - can_ip_forward: false
  - network_interfaces:
    - access_configs:
      - kind: compute#accessConfig
        name: External NAT
        network_tier: PREMIUM
        set_public_ptr: false
        type: ONE_TO_ONE_NAT
      kind: compute#networkInterface
      name: nic0
      network: https://www.googleapis.com/compute/v1/projects/{{project_id}}/global/networks/default
      stack_type: IPV4_ONLY
      subnetwork: https://www.googleapis.com/compute/v1/projects/{{project_id}}/regions/us-central1/subnetworks/default
  - disks:
    - auto_delete: true
      boot: true
      initialize_params:
          disk_size_gb: "10"
          disk_type: "http://localhost:8000/projects/{{project_id}}/zones/us-central1-a/diskTypes/pd-balanced"
          source_image: "http://localhost:8000/projects/{{project_id}}/global/images/debian-11-bullseye-v20221102"
      device_name: example_disk_1
      source: https://www.googleapis.com/compute/v1/projects/{{project_id}}/zones/us-central1-a/disks/example_disk_1
      mode: READ_WRITE
      type: PERSISTENT
      disk_size_gb: '10'
      index: 0
      interface: SCSI
      kind: compute#attachedDisk
  - scheduling:
      automatic_restart: true
      on_host_maintenance: MIGRATE
      preemptible: false
      provisioning_model: STANDARD
  - deletion_protection: false
  - tags:
      items:
        - test
  - metadata:
      kind: compute#metadata
      items:
        - key: sample_metadata_key
          value: sample_metadata_value

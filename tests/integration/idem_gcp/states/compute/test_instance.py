import asyncio
import copy
import time
from collections import ChainMap
from typing import Any
from typing import Dict

import pytest
import pytest_asyncio
import yaml
from dict_tools.data import NamespaceDict

from tests.utils import create_external_instance
from tests.utils import delete_instance
from tests.utils import generate_unique_name
from tests.utils import stop_instance
from tests.utils import wait_for_operation

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": generate_unique_name("idem-test-instance"),
    "disk_name": generate_unique_name("idem-test-disk"),
    "network_name": generate_unique_name("idem-test-network"),
    "subnetwork_name": generate_unique_name("idem-test-subnetwork"),
    "subnetwork_cidr_range": "10.200.0.0/20",
    "zone": "us-central1-a",
    "region": "us-central1",
    "project": "tango-gcp",
    "external_instance_name": generate_unique_name("idem-test-ext-instance"),
    "external_instance_for_reconcile_name": generate_unique_name("idem-test-reconcile"),
    "instance_get_resource_only_with_resource_id": generate_unique_name(
        "idem-test-absent-instance"
    ),
    "timestamp": str(int(time.time())),
}

INSTANCE_CURRENT_STATE = None

PRESENT_STATE_DISK = {
    "name": PARAMETER["disk_name"],
    "zone": PARAMETER["zone"],
    "project": PARAMETER["project"],
    "type_": f"projects/{PARAMETER['project']}/zones/{PARAMETER['zone']}/diskTypes/pd-balanced",
    "size_gb": "10",
    "physical_block_size_bytes": "4096",
    "resource_policies": [
        f"projects/{PARAMETER['project']}/regions/{PARAMETER['region']}/resourcePolicies/default-schedule-1"
    ],
    "guest_os_features": [{"type_": "UEFI_COMPATIBLE"}],
}


PRESENT_STATE_NETWORK = {
    "name": PARAMETER["network_name"],
    "auto_create_subnetworks": False,
    "routing_config": {"routing_mode": "REGIONAL"},
}

PRESENT_STATE_SUBNETWORK = {
    "name": PARAMETER["subnetwork_name"],
    "network": "{network_resource_id}",
    "region": PARAMETER["region"],
    "ip_cidr_range": PARAMETER["subnetwork_cidr_range"],
    "stack_type": "IPV4_ONLY",
}


PRESENT_CREATE_STATE = {
    "name": PARAMETER["name"],
    "project": PARAMETER["project"],
    "zone": PARAMETER["zone"],
    "machine_type": f"projects/{PARAMETER['project']}/zones/{PARAMETER['zone']}/machineTypes/g1-small",
    "can_ip_forward": False,
    "network_interfaces": [
        {
            "kind": "compute#networkInterface",
            "name": "nic0",
            "network": f"projects/{PARAMETER['project']}/global/networks/default",
            "stack_type": "IPV4_ONLY",
            "subnetwork": f"projects/{PARAMETER['project']}/regions/{PARAMETER['region']}/subnetworks/default",
        }
    ],
    "disks": [
        {
            "auto_delete": True,
            "boot": True,
            "device_name": PARAMETER["disk_name"],
            "source": f"projects/{PARAMETER['project']}/zones/{PARAMETER['zone']}/disks/{PARAMETER['disk_name']}",
            "mode": "READ_WRITE",
            "type_": "PERSISTENT",
            "disk_size_gb": "10",
            "index": 0,
            "interface": "SCSI",
            "kind": "compute#attachedDisk",
            "guest_os_features": [{"type_": "UEFI_COMPATIBLE"}],
        }
    ],
    "scheduling": {
        "automatic_restart": True,
        "on_host_maintenance": "MIGRATE",
        "preemptible": False,
        "provisioning_model": "STANDARD",
    },
    "deletion_protection": False,
    "tags": {"items": ["test"]},
    "metadata": {
        "kind": "compute#metadata",
        "items": [{"key": "sample_metadata_key", "value": "sample_metadata_value"}],
    },
    "shielded_instance_config": {
        "enable_secure_boot": False,
        "enable_vtpm": True,
        "enable_integrity_monitoring": True,
    },
    "shielded_instance_integrity_policy": {"update_auto_learn_policy": True},
    "status": "RUNNING",
    "labels": {"first_label_key": "first_label_value"},
}

DESCRIPTION = "Only description is updated."

PRESENT_UPDATED_DESCRIPTION = {
    "description": DESCRIPTION,
}

PRESENT_UPDATED_PROPERTIES = {
    "description": "test description",
    "tags": {"items": ["test1", "test2", "test3"]},
    "can_ip_forward": True,
    "network_interfaces": [
        {
            "access_configs": [
                {
                    "kind": "compute#accessConfig",
                    "name": "External NAT",
                    "network_tier": "PREMIUM",
                    "set_public_ptr": False,
                    "type_": "ONE_TO_ONE_NAT",
                }
            ],
            "kind": "compute#networkInterface",
            "name": "nic0",
            "network": f"projects/{PARAMETER['project']}/global/networks/default",
            "stack_type": "IPV4_ONLY",
            "subnetwork": f"projects/{PARAMETER['project']}/regions/{PARAMETER['region']}/subnetworks/default",
        }
    ],
    "labels": {
        "first_label_key": "first_label_value",
        "second_label_key": "second_label_value",
    },
}

PRESENT_UPDATED_LABELS = {
    "labels": {"second_label_key": "second_label_value"},
}

PRESENT_UPDATED_SHIELDED_CONFIG = {
    "shielded_instance_config": {
        "enable_secure_boot": True,
        "enable_vtpm": False,
        "enable_integrity_monitoring": False,
    }
}

PRESENT_UPDATED_NON_UPDATABLE_PROPERTIES = {
    "id_": "attempt-to-update",
    "disks": [
        {
            "initialize_params": {
                "disk_name": "test_disk_name",
                "source_image": "projects/debian-cloud/global/images/family/debian-11",
                "disk_size_gb": "10",
            }
        }
    ],
}

NON_UPDATABLE_PROPERTIES_PATHS = {"disks[].initialize_params", "id_"}

RESOURCE_ID = f"projects/{PARAMETER['project']}/zones/{PARAMETER['zone']}/instances/{PARAMETER['name']}"

PRESENT_WITH_RESOURCE_ID = {
    "resource_id": RESOURCE_ID,
}

RESOURCE_TYPE_INSTANCE = "compute.instance"
GCP_RESOURCE_TYPE_INSTANCE = "gcp.compute.instance"
RESOURCE_TYPE_DISK = "compute.disk"
RESOURCE_TYPE_NETWORK = "compute.network"
RESOURCE_TYPE_SUBNETWORK = "compute.subnetwork"

EXTERNAL_INSTANCE_SPEC = f"""
{PARAMETER["external_instance_name"]}:
  gcp.compute.instance.present:
  - name: {PARAMETER["external_instance_name"]}
  - project: {PARAMETER['project']}
  - machine_type: projects/{PARAMETER['project']}/zones/{PARAMETER['zone']}/machineTypes/g1-small
  - zone: {PARAMETER['zone']}
  - network_interfaces:
    - access_configs:
      - kind: compute#accessConfig
        name: External NAT
        type_: ONE_TO_ONE_NAT
      kind: compute#networkInterface
      network: projects/{PARAMETER['project']}/global/networks/default
  - disks:
    - auto_delete: true
      boot: true
"""

# Data format { "the_resource_id": { "name": "name", "resource_type": "type" } }
RESOURCES_TO_DELETE = {}


@pytest.fixture(scope="module")
def new_disk(hub, idem_cli):
    disk_present_ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_DISK, PRESENT_STATE_DISK
    )
    assert disk_present_ret["result"], disk_present_ret["comment"]

    resource_id = disk_present_ret["new_state"]["resource_id"]
    PRESENT_STATE_DISK["id_"] = disk_present_ret["new_state"]["id_"]
    expected_state = {"resource_id": resource_id, **PRESENT_STATE_DISK}
    # project is a path parameter, not part of the resource body
    expected_state.pop("project")
    changes = hub.tool.gcp.utils.compare_states(
        disk_present_ret["new_state"], expected_state, RESOURCE_TYPE_DISK
    )
    assert not bool(changes), changes

    yield disk_present_ret

    disk_absent_ret = hub.tool.utils.call_absent(
        idem_cli, "compute.disk", disk_present_ret["new_state"]["name"], resource_id
    )
    assert disk_absent_ret["result"], disk_absent_ret["comment"]
    assert not disk_absent_ret.get("new_state")


@pytest.fixture(scope="module")
def new_network(hub, idem_cli):
    network_present_ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_NETWORK, PRESENT_STATE_NETWORK
    )
    assert network_present_ret["result"], network_present_ret["comment"]

    resource_id = network_present_ret["new_state"]["resource_id"]
    assert resource_id

    yield network_present_ret["new_state"]

    network_absent_ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE_NETWORK,
        network_present_ret["new_state"]["name"],
        resource_id,
    )
    assert network_absent_ret["result"], network_absent_ret["comment"]
    assert not network_absent_ret.get("new_state")


@pytest.fixture(scope="module")
def new_subnetwork(hub, idem_cli, new_network):
    PRESENT_STATE_SUBNETWORK["network"] = PRESENT_STATE_SUBNETWORK["network"].format(
        network_resource_id=new_network["resource_id"]
    )
    subnetwork_present_ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_SUBNETWORK, PRESENT_STATE_SUBNETWORK
    )
    assert subnetwork_present_ret["result"], subnetwork_present_ret["comment"]

    resource_id = subnetwork_present_ret["new_state"]["resource_id"]
    assert resource_id

    yield subnetwork_present_ret["new_state"]

    subnetwork_absent_ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE_SUBNETWORK,
        subnetwork_present_ret["new_state"]["name"],
        resource_id,
    )
    assert subnetwork_absent_ret["result"], subnetwork_absent_ret["comment"]
    assert not subnetwork_absent_ret.get("new_state")


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present_create")
def test_instance_present_create(hub, idem_cli, tests_dir, __test, new_disk):
    global PARAMETER
    global INSTANCE_CURRENT_STATE
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_INSTANCE, PRESENT_CREATE_STATE, __test
    )

    assert ret["result"], ret["comment"]
    if __test:
        assert (
            hub.tool.gcp.comment_utils.would_create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCE}", name=PARAMETER["name"]
            )
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.gcp.comment_utils.create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCE}", name=PARAMETER["name"]
            )
            in ret["comment"]
        )
        RESOURCES_TO_DELETE[ret["new_state"]["resource_id"]] = {
            "name": PARAMETER["name"],
            "resource_type": RESOURCE_TYPE_INSTANCE,
        }
        assert ret["new_state"]["id_"]
        PRESENT_CREATE_STATE["id_"] = ret["new_state"]["id_"]
    expected_state = {"resource_id": RESOURCE_ID, **PRESENT_CREATE_STATE}
    # project is a path parameter, not part of the resource body
    expected_state.pop("project")
    changes = hub.tool.gcp.utils.compare_states(
        ret["new_state"], expected_state, RESOURCE_TYPE_INSTANCE
    )
    assert not bool(changes), changes
    if not __test:
        INSTANCE_CURRENT_STATE = ret["new_state"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="present_get_resource_only_with_resource_id", depends=["present_create"]
)
def test_instance_present_get_resource_only_with_resource_id(
    hub, idem_cli, tests_dir, __test
):
    present_state = copy.deepcopy(PRESENT_CREATE_STATE)
    present_state.update(PRESENT_WITH_RESOURCE_ID)
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE_INSTANCE,
        present_state,
        __test,
        ["--get-resource-only-with-resource-id"],
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.gcp.comment_utils.up_to_date_comment(
            resource_type=f"gcp.{RESOURCE_TYPE_INSTANCE}", name=PARAMETER["name"]
        )
        in ret["comment"]
    )

    expected_state = {"resource_id": RESOURCE_ID, **present_state}
    expected_state.pop("project")
    changes = hub.tool.gcp.utils.compare_states(
        ret["new_state"], expected_state, RESOURCE_TYPE_INSTANCE
    )

    assert not bool(changes), changes
    assert ret["new_state"] == ret["old_state"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="present_get_resource_only_with_resource_id_missing_resource_id",
    depends=["present_create"],
)
def test_instance_present_get_resource_only_with_resource_id_missing_resource_id(
    hub, idem_cli, tests_dir, __test
):
    present_state = copy.deepcopy(PRESENT_CREATE_STATE)
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE_INSTANCE,
        present_state,
        __test,
        ["--get-resource-only-with-resource-id"],
    )

    if __test:
        assert ret["result"], ret["comment"]
        assert (
            hub.tool.gcp.comment_utils.would_create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCE}", name=PARAMETER["name"]
            )
            in ret["comment"]
        )
    else:
        assert not ret["result"], ret["comment"]
        assert (
            hub.tool.gcp.comment_utils.already_exists_comment(
                GCP_RESOURCE_TYPE_INSTANCE, name=PARAMETER["name"]
            )
            in ret["comment"]
        )


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="dont_update_get_resource_only_with_resource_id_missing_resource_id",
    depends=["present_create"],
)
def test_instance_present_dont_update_get_resource_only_with_resource_id_missing_resource_id(
    hub, idem_cli, tests_dir, __test
):
    present_state = copy.deepcopy(PRESENT_CREATE_STATE)
    present_state.update(PRESENT_UPDATED_PROPERTIES)
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE_INSTANCE,
        present_state,
        __test,
        ["--get-resource-only-with-resource-id"],
    )

    if __test:
        assert ret["result"], ret["comment"]
        assert (
            hub.tool.gcp.comment_utils.would_create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCE}", name=PARAMETER["name"]
            )
            in ret["comment"]
        )
    else:
        assert not ret["result"], ret["comment"]
        assert (
            hub.tool.gcp.comment_utils.already_exists_comment(
                GCP_RESOURCE_TYPE_INSTANCE, name=PARAMETER["name"]
            )
            in ret["comment"]
        )


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="update_get_resource_only_with_resource_id_missing_resource_id",
    depends=["present_create"],
)
def test_instance_present_update_get_resource_only_with_resource_id_with_resource_id(
    hub, idem_cli, tests_dir, __test
):
    global INSTANCE_CURRENT_STATE

    update_present_state = copy.deepcopy(INSTANCE_CURRENT_STATE)
    update_present_state.update(PRESENT_UPDATED_DESCRIPTION)

    ret = hub.tool.utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE_INSTANCE,
        update_present_state,
        __test,
        ["--get-resource-only-with-resource-id"],
    )
    assert ret["result"], ret["comment"]
    if __test:
        assert (
            hub.tool.gcp.comment_utils.would_update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCE}", name=PARAMETER["name"]
            )
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.gcp.comment_utils.update_comment(
                GCP_RESOURCE_TYPE_INSTANCE, name=PARAMETER["name"]
            )
            in ret["comment"]
        )

    expected_new_state_result = hub.tool.gcp.utils.compare_states(
        ret["new_state"], update_present_state, RESOURCE_TYPE_INSTANCE
    )
    expected_old_state_result = hub.tool.gcp.utils.compare_states(
        ret["old_state"], INSTANCE_CURRENT_STATE, RESOURCE_TYPE_INSTANCE
    )
    assert not bool(expected_new_state_result), expected_new_state_result
    assert not bool(expected_old_state_result), expected_old_state_result
    if not __test:
        INSTANCE_CURRENT_STATE = ret["new_state"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="present_update",
    depends=["update_get_resource_only_with_resource_id_missing_resource_id"],
)
def test_instance_present_update(hub, idem_cli, tests_dir, __test):
    global INSTANCE_CURRENT_STATE
    update_present_state = copy.deepcopy(INSTANCE_CURRENT_STATE)
    update_present_state.update(PRESENT_UPDATED_PROPERTIES)
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_INSTANCE, update_present_state, __test
    )

    assert ret["result"], ret["comment"]
    if __test:
        assert (
            hub.tool.gcp.comment_utils.would_update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCE}", name=PARAMETER["name"]
            )
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.gcp.comment_utils.update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCE}", name=PARAMETER["name"]
            )
            in ret["comment"]
        )

    expected_new_state_result = hub.tool.gcp.utils.compare_states(
        ret["new_state"], update_present_state, RESOURCE_TYPE_INSTANCE
    )
    expected_old_state_result = hub.tool.gcp.utils.compare_states(
        ret["old_state"], INSTANCE_CURRENT_STATE, RESOURCE_TYPE_INSTANCE
    )
    assert not bool(expected_new_state_result), expected_new_state_result
    assert not bool(expected_old_state_result), expected_old_state_result
    if not __test:
        INSTANCE_CURRENT_STATE = ret["new_state"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="present_update_warn_props_mismatch",
    depends=["present_update"],
)
def test_instance_present_empty_update_props_mismatch(hub, idem_cli, tests_dir, __test):
    update_present_state = copy.deepcopy(INSTANCE_CURRENT_STATE)
    update_present_state["project"] = "project-gcp"
    update_present_state["resource_id"] = RESOURCE_ID

    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_INSTANCE, update_present_state, __test
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.gcp.comment_utils.properties_mismatch_resource_id_comment(
            GCP_RESOURCE_TYPE_INSTANCE, PARAMETER["name"]
        )
        in ret["comment"]
    )
    assert (
        hub.tool.gcp.comment_utils.up_to_date_comment(
            resource_type=f"gcp.{RESOURCE_TYPE_INSTANCE}", name=PARAMETER["name"]
        )
        in ret["comment"]
    )

    no_changes_result = hub.tool.gcp.utils.compare_states(
        ret["old_state"], INSTANCE_CURRENT_STATE, RESOURCE_TYPE_INSTANCE
    )
    assert not bool(no_changes_result), no_changes_result


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="present_remove_labels",
    depends=["present_update"],
)
def test_instance_present_remove_labels(hub, idem_cli, tests_dir, __test):
    global INSTANCE_CURRENT_STATE
    update_present_state = copy.deepcopy(INSTANCE_CURRENT_STATE)

    old_labels = update_present_state.get("labels", {})
    update_present_state.update(PRESENT_UPDATED_LABELS)
    new_labels = update_present_state.get("labels", {})

    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_INSTANCE, update_present_state, __test
    )

    assert ret["result"], ret["comment"]
    if __test:
        assert (
            hub.tool.gcp.comment_utils.would_update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCE}", name=PARAMETER["name"]
            )
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.gcp.comment_utils.update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCE}", name=PARAMETER["name"]
            )
            in ret["comment"]
        )

    old_labels_compare_result = ret["old_state"].get("labels", {}) == old_labels
    new_labels_compare_result = ret["new_state"].get("labels", {}) == new_labels

    assert (
        old_labels_compare_result
    ), f"Old labels differ: expected:{old_labels} != actual:{ret['old_state'].get('labels', {})}"
    assert (
        new_labels_compare_result
    ), f"New labels differ: expected:{new_labels} != actual:{ret['new_state'].get('labels', {})}"

    if not __test:
        INSTANCE_CURRENT_STATE = ret["new_state"]


@pytest.mark.dependency(
    name="access_config_empty_update",
    depends=["present_update"],
)
def test_instance_present_update_no_access_configs_empty_update(
    hub, idem_cli, tests_dir
):
    old_network_interface = INSTANCE_CURRENT_STATE["network_interfaces"][0]
    new_network_interface = copy.copy(old_network_interface)
    assert new_network_interface.pop("access_configs")
    update_present_state = copy.deepcopy(INSTANCE_CURRENT_STATE)
    update_present_state.update({"network_interfaces": [new_network_interface]})
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_INSTANCE, update_present_state
    )

    assert ret["result"], ret["comment"]
    assert [
        hub.tool.gcp.comment_utils.up_to_date_comment(
            resource_type=f"gcp.{RESOURCE_TYPE_INSTANCE}", name=PARAMETER["name"]
        )
    ] == ret["comment"]

    changes = hub.tool.gcp.utils.compare_states(
        ret["new_state"], INSTANCE_CURRENT_STATE, RESOURCE_TYPE_INSTANCE
    )

    assert not bool(changes), changes
    assert ret["new_state"] == ret["old_state"]


@pytest.mark.dependency(
    depends=["access_config_empty_update"],
)
def test_update_network_interface_wrong_nat_ip(hub, ctx, idem_cli):
    global INSTANCE_CURRENT_STATE
    old_network_interface = INSTANCE_CURRENT_STATE["network_interfaces"][0]
    # delete access config and update network
    old_access_configs = old_network_interface.get("access_configs")
    assert old_access_configs
    old_access_config = old_access_configs[0]

    updated_network_interface = {
        **old_network_interface,
        # invalid nat_ip
        "access_configs": [{**old_access_config, "nat_ip": "5.5.5.5"}],
        # too big queue count
        "queue_count": 100,
    }
    update_present_state = copy.deepcopy(INSTANCE_CURRENT_STATE)
    update_present_state.update({"network_interfaces": [updated_network_interface]})
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_INSTANCE, update_present_state
    )

    assert ret["result"] is False
    # old access config has been deleted, new one hasn't been added since it's invalid
    expected_old_state_result = hub.tool.gcp.utils.compare_states(
        ret["old_state"], INSTANCE_CURRENT_STATE, RESOURCE_TYPE_INSTANCE
    )
    assert not bool(expected_old_state_result), expected_old_state_result

    assert len(ret["old_state"]["network_interfaces"]) == 1
    assert len(ret["new_state"]["network_interfaces"]) == 1

    assert ret["old_state"]["network_interfaces"][0]["access_configs"]
    assert len(ret["old_state"]["network_interfaces"][0]["access_configs"]) == 1
    assert (
        ret["old_state"]["network_interfaces"][0]["access_configs"][0]["nat_ip"]
        == old_access_config["nat_ip"]
    )

    assert not ret["new_state"]["network_interfaces"][0].get("access_configs")

    expected_new_state = copy.deepcopy(INSTANCE_CURRENT_STATE)
    expected_new_state["network_interfaces"][0].pop("access_configs", None)
    # compare that everything but access configs is equal to before
    no_changes_result = hub.tool.gcp.utils.compare_states(
        ret["new_state"], expected_new_state, RESOURCE_TYPE_INSTANCE
    )
    assert not bool(no_changes_result), no_changes_result

    assert ret["comment"]
    assert "Invalid value for field 'resource.natIP'" in ret["comment"][0]

    INSTANCE_CURRENT_STATE = ret["new_state"]


@pytest.mark.dependency(
    depends=["present_update"],
)
def test_instance_present_update_two_access_configs_fails(hub, idem_cli, tests_dir):
    old_network_interface = INSTANCE_CURRENT_STATE["network_interfaces"][0]
    access_config_value = {
        "kind": "compute#accessConfig",
        "name": "External NAT",
        "network_tier": "PREMIUM",
        "set_public_ptr": False,
        "type_": "ONE_TO_ONE_NAT",
    }
    # add two access configs
    updated_network_interface = {
        **old_network_interface,
        "access_configs": [access_config_value, access_config_value],
    }
    update_present_state = copy.deepcopy(INSTANCE_CURRENT_STATE)
    update_present_state.update({"network_interfaces": [updated_network_interface]})
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_INSTANCE, update_present_state
    )

    assert ret["result"] is False
    assert [
        f"Network interface {updated_network_interface['name']} cannot have more than one access config"
    ] == ret["comment"]
    expected_old_state_result = hub.tool.gcp.utils.compare_states(
        ret["old_state"], INSTANCE_CURRENT_STATE, RESOURCE_TYPE_INSTANCE
    )
    assert not bool(expected_old_state_result), expected_old_state_result
    assert ret["new_state"] == ret["old_state"]


@pytest.mark.dependency(
    depends=["present_update"],
)
def test_instance_present_update_add_network_interface_fails(hub, idem_cli, tests_dir):
    old_network_interface = INSTANCE_CURRENT_STATE["network_interfaces"][0]
    # add another network interface - not supported
    new_network_interface = {**old_network_interface, "name": "invalid-new-interface"}
    update_present_state = copy.deepcopy(INSTANCE_CURRENT_STATE)
    update_present_state.update(
        {"network_interfaces": [old_network_interface, new_network_interface]}
    )
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_INSTANCE, update_present_state
    )

    assert ret["result"] is False
    assert [
        f"Network interface addition not supported for interface {new_network_interface['name']}"
    ] == ret["comment"]
    expected_old_state_result = hub.tool.gcp.utils.compare_states(
        ret["old_state"], INSTANCE_CURRENT_STATE, RESOURCE_TYPE_INSTANCE
    )
    assert not bool(expected_old_state_result), expected_old_state_result
    assert ret["new_state"] == ret["old_state"]


@pytest.mark.dependency(
    depends=["present_update"],
)
def test_instance_present_update_delete_network_interface_fails(
    hub, idem_cli, tests_dir
):
    old_network_interface = INSTANCE_CURRENT_STATE["network_interfaces"][0]
    update_present_state = copy.deepcopy(INSTANCE_CURRENT_STATE)
    update_present_state.update({"network_interfaces": []})
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_INSTANCE, update_present_state
    )

    assert ret["result"] is False
    assert [
        f"Network interface deletion not supported for interface {old_network_interface['name']}"
    ] == ret["comment"]
    expected_old_state_result = hub.tool.gcp.utils.compare_states(
        ret["old_state"], INSTANCE_CURRENT_STATE, RESOURCE_TYPE_INSTANCE
    )
    assert not bool(expected_old_state_result), expected_old_state_result
    assert ret["new_state"] == ret["old_state"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present_already_exists", depends=["present_update"])
def test_instance_present_already_exists(hub, idem_cli, tests_dir, __test):
    global PARAMETER
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_INSTANCE, INSTANCE_CURRENT_STATE, __test
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.gcp.comment_utils.up_to_date_comment(
            resource_type=f"gcp.{RESOURCE_TYPE_INSTANCE}", name=PARAMETER["name"]
        )
        in ret["comment"]
    )

    changes = hub.tool.gcp.utils.compare_states(
        ret["new_state"], INSTANCE_CURRENT_STATE, RESOURCE_TYPE_INSTANCE
    )

    assert not bool(changes), changes
    assert ret["new_state"] == ret["old_state"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="present_changed_non_updatable_properties", depends=["present_already_exists"]
)
def test_instance_present_changed_non_updatable_properties(
    hub, idem_cli, tests_dir, __test
):
    global PARAMETER
    update_present_state = copy.deepcopy(INSTANCE_CURRENT_STATE)
    update_present_state.update(PRESENT_UPDATED_NON_UPDATABLE_PROPERTIES)
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_INSTANCE, update_present_state, __test
    )

    assert not ret["result"], ret["comment"]
    assert [
        hub.tool.gcp.comment_utils.non_updatable_properties_comment(
            f"gcp.{RESOURCE_TYPE_INSTANCE}",
            PARAMETER["name"],
            NON_UPDATABLE_PROPERTIES_PATHS,
        )
    ] == ret["comment"]

    changes = hub.tool.gcp.utils.compare_states(
        ret["new_state"], INSTANCE_CURRENT_STATE, RESOURCE_TYPE_INSTANCE
    )

    assert not bool(changes), changes
    assert ret["new_state"] == ret["old_state"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="instance_present_update_shielded_config_after_power_off",
    depends=["present_create"],
)
async def test_instance_present_update_shielded_config_after_power_off(
    hub, ctx, idem_cli, tests_dir, __test
):
    global PARAMETER
    global INSTANCE_CURRENT_STATE

    if not __test:
        project = PARAMETER["project"]
        zone = PARAMETER["zone"]
        name = PARAMETER["name"]
        op = stop_instance(ctx, project, zone, name)
        wait_for_operation(ctx, project, zone, op)

    expected_old_state = await hub.exec.gcp.compute.instance.get(
        ctx, resource_id=RESOURCE_ID
    )
    INSTANCE_CURRENT_STATE = expected_old_state["ret"]

    update_present_state = copy.deepcopy(INSTANCE_CURRENT_STATE)
    update_present_state.update(PRESENT_UPDATED_SHIELDED_CONFIG)

    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_INSTANCE, update_present_state, __test
    )

    assert ret["result"], ret["comment"]
    if __test:
        assert (
            hub.tool.gcp.comment_utils.would_update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCE}", name=PARAMETER["name"]
            )
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.gcp.comment_utils.update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCE}", name=PARAMETER["name"]
            )
            in ret["comment"]
        )

    expected_old_state_result = hub.tool.gcp.utils.compare_states(
        ret["old_state"],
        INSTANCE_CURRENT_STATE,
        RESOURCE_TYPE_INSTANCE,
    )
    assert not bool(expected_old_state_result), expected_old_state_result

    expected_new_state_result = hub.tool.gcp.utils.compare_states(
        ret["new_state"], update_present_state, RESOURCE_TYPE_INSTANCE
    )
    assert not bool(expected_new_state_result), expected_new_state_result

    if not __test:
        INSTANCE_CURRENT_STATE = ret["new_state"]


# to change the network/subnetwork the VM needs to be in STOPPED state
@pytest.mark.dependency(
    name="update_subnetwork",
    depends=["instance_present_update_shielded_config_after_power_off"],
)
def test_update_network_interface_subnetwork_and_access_config(
    hub, idem_cli, new_subnetwork
):
    global INSTANCE_CURRENT_STATE
    old_network_interface = INSTANCE_CURRENT_STATE["network_interfaces"][0]
    updated_network_interface = {
        **old_network_interface,
        "access_configs": [],
        "network": new_subnetwork["network"],
        "subnetwork": f"{new_subnetwork['resource_id']}",
        # remove network_ip as this is an IP inside the old range
        # and would be invalid in the new one
        "network_ip": None,
    }
    update_present_state = copy.deepcopy(INSTANCE_CURRENT_STATE)
    update_present_state.update({"network_interfaces": [updated_network_interface]})
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_INSTANCE, update_present_state
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.gcp.comment_utils.update_comment(
            resource_type=f"gcp.{RESOURCE_TYPE_INSTANCE}", name=PARAMETER["name"]
        )
        in ret["comment"]
    )

    expected_old_state_result = hub.tool.gcp.utils.compare_states(
        ret["old_state"], INSTANCE_CURRENT_STATE, RESOURCE_TYPE_INSTANCE
    )
    assert not bool(expected_old_state_result), expected_old_state_result
    expected_new_state_result = hub.tool.gcp.utils.compare_states(
        ret["new_state"],
        update_present_state,
        RESOURCE_TYPE_INSTANCE,
        additional_exclude_paths=["network_interfaces[].network_ip"],
    )
    assert not bool(expected_new_state_result), expected_new_state_result
    assert (
        ret["new_state"]
        .get("network_interfaces")[0]
        .get("network_ip")
        .startswith(PARAMETER["subnetwork_cidr_range"][:7])
    )
    INSTANCE_CURRENT_STATE = ret["new_state"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="instance_lifecycle_power_on_after_power_off",
    depends=["update_subnetwork"],
)
async def test_instance_lifecycle_power_on_after_power_off(
    hub, ctx, idem_cli, tests_dir, __test
):
    global PARAMETER
    global INSTANCE_CURRENT_STATE

    ret = await hub.exec.gcp.compute.instance.get(ctx, resource_id=RESOURCE_ID)
    assert ret["result"], ret["comment"]

    instance = ret["ret"]
    assert instance["status"] == "TERMINATED"

    instance["status"] = "RUNNING"
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_INSTANCE, instance, __test
    )

    if __test:
        assert (
            hub.tool.gcp.comment_utils.would_update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCE}", name=PARAMETER["name"]
            )
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.gcp.comment_utils.update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCE}", name=PARAMETER["name"]
            )
            in ret["comment"]
        )

    assert ret["new_state"]
    assert ret["new_state"]["status"] == "RUNNING"
    if not __test:
        INSTANCE_CURRENT_STATE = ret["new_state"]
    assert ret["old_state"]
    assert ret["old_state"]["status"] == "TERMINATED"


@pytest.mark.asyncio
def test_instance_create_in_terminated_state(hub, idem_cli, tests_dir, cleaner):
    global PARAMETER

    timestamp = int(time.time())
    path_to_sls = tests_dir / "sls" / "compute" / "default_instance_present.sls"
    with open(path_to_sls) as template:
        data_template = template.read()
        sls_str = data_template.format(**{"timestamp": timestamp})

    sls_str = sls_str.replace("status: RUNNING", "status: TERMINATED")
    ret = hub.tool.utils.call_present_from_sls(idem_cli, sls_str)

    assert ret["result"], ret["comment"]
    cleaner(ret["new_state"], RESOURCE_TYPE_INSTANCE)

    assert (
        hub.tool.gcp.comment_utils.create_comment(
            resource_type=f"gcp.{RESOURCE_TYPE_INSTANCE}",
            name=f"idem-fixture-instance-test-{timestamp}",
        )
        in ret["comment"]
    )
    assert ret["new_state"]["status"] == "TERMINATED"


@pytest.mark.asyncio
@pytest.mark.dependency(
    name="describe", depends=["present_changed_non_updatable_properties"]
)
async def test_instance_describe(hub, ctx):
    describe_ret = await hub.states.gcp.compute.instance.describe(ctx)
    assert RESOURCE_ID in describe_ret
    assert f"gcp.{RESOURCE_TYPE_INSTANCE}.present" in describe_ret[RESOURCE_ID]
    described_resource = describe_ret[RESOURCE_ID].get(
        f"gcp.{RESOURCE_TYPE_INSTANCE}.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))

    assert PARAMETER["name"] == described_resource_map["name"]
    assert RESOURCE_ID == described_resource_map["resource_id"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="absent_get_resource_only_with_resource_id_missing_resource_id",
    depends=["describe"],
)
def test_instance_absent_get_resource_only_with_resource_id_missing_resource_id(
    hub, idem_cli, tests_dir, __test
):
    global PARAMETER
    ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE_INSTANCE,
        PARAMETER["name"],
        None,
        test=__test,
        additional_kwargs=["--get-resource-only-with-resource-id"],
    )

    absent_comment = hub.tool.gcp.comment_utils.already_absent_comment(
        GCP_RESOURCE_TYPE_INSTANCE, PARAMETER["name"]
    )
    assert any(absent_comment in c for c in ret["comment"])
    assert ret["result"], ret["comment"]
    assert not ret.get("new_state")
    assert not ret.get("old_state")


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="absent",
    depends=["absent_get_resource_only_with_resource_id_missing_resource_id"],
)
def test_instance_absent(hub, idem_cli, tests_dir, __test):
    global PARAMETER
    ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE_INSTANCE,
        PARAMETER["name"],
        RESOURCE_ID,
        test=__test,
    )

    assert ret["result"], ret["comment"]
    if __test:
        assert (
            hub.tool.gcp.comment_utils.would_delete_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCE}", name=PARAMETER["name"]
            )
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.gcp.comment_utils.delete_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_INSTANCE}", name=PARAMETER["name"]
            )
            in ret["comment"]
        )
        RESOURCES_TO_DELETE.pop(RESOURCE_ID, None)

    changes = hub.tool.gcp.utils.compare_states(
        ret["old_state"], INSTANCE_CURRENT_STATE, RESOURCE_TYPE_INSTANCE
    )

    assert not bool(changes), changes
    assert not ret.get("new_state")


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
def test_instance_already_absent(hub, idem_cli, tests_dir, __test):
    global PARAMETER
    ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE_INSTANCE,
        PARAMETER["name"],
        RESOURCE_ID,
        test=__test,
    )

    absent_comment = hub.tool.gcp.comment_utils.already_absent_comment(
        GCP_RESOURCE_TYPE_INSTANCE, PARAMETER["name"]
    )
    assert any(absent_comment in c for c in ret["comment"])
    assert ret["result"], ret["comment"]
    assert not ret.get("new_state")
    assert not ret.get("old_state")


@pytest.mark.dependency(name="test_instance_dont_absent_only_by_name")
@pytest.mark.asyncio
def test_instance_dont_absent_only_by_name(hub, ctx, idem_cli, tests_dir):
    global PARAMETER
    project = PARAMETER["project"]
    zone = PARAMETER["zone"]
    name = PARAMETER["instance_get_resource_only_with_resource_id"]

    # creates a GCP instance using the GCP API client library without going through idem
    create_external_instance(ctx, project, zone, name)

    ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE_INSTANCE,
        name,
        resource_id=None,
        test=False,
    )

    absent_comment = hub.tool.gcp.comment_utils.already_absent_comment(
        GCP_RESOURCE_TYPE_INSTANCE, name
    )
    assert any(absent_comment in c for c in ret["comment"]), ret["comment"]
    assert ret["result"], ret["comment"]
    assert not ret.get("new_state")
    assert not ret.get("old_state")


@pytest.mark.dependency(
    name="test_instance_absent_by_name_zone_project",
    depends=["test_instance_dont_absent_only_by_name"],
)
@pytest.mark.asyncio
def test_instance_absent_by_name_zone_project(hub, ctx, idem_cli, tests_dir):
    global PARAMETER
    project = PARAMETER["project"]
    zone = PARAMETER["zone"]
    name = PARAMETER["instance_get_resource_only_with_resource_id"]

    ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE_INSTANCE,
        name,
        None,
        test=False,
        zone=zone,
        project=project,
    )

    assert [
        hub.tool.gcp.comment_utils.delete_comment(
            resource_type=f"gcp.{RESOURCE_TYPE_INSTANCE}", name=name
        )
    ] == ret["comment"]
    assert ret["result"], ret["comment"]
    assert not ret.get("new_state")
    assert ret.get("old_state")


@pytest.mark.asyncio
async def test_discover_external_instance(hub, ctx, idem_cli, tests_dir):
    project = PARAMETER["project"]
    zone = PARAMETER["zone"]
    name = PARAMETER["external_instance_name"]

    # creates a GCP instance using the GCP API client library without going through idem
    create_external_instance(ctx, project, zone, name)

    ret = hub.tool.utils.call_present_from_sls(idem_cli, EXTERNAL_INSTANCE_SPEC)

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.gcp.comment_utils.up_to_date_comment(GCP_RESOURCE_TYPE_INSTANCE, name)
        in ret["comment"]
    )
    assert ret["new_state"] == ret["old_state"]
    assert (
        ret["new_state"]["resource_id"]
        == f"projects/{project}/zones/{zone}/instances/{name}"
    )

    # deletes the created instance using the GCP API client library
    delete_instance(ctx, project, zone, name)


@pytest.mark.asyncio
async def test_absent_reconcile_output(hub, ctx, idem_cli, tests_dir):
    project = PARAMETER["project"]
    zone = PARAMETER["zone"]
    name = PARAMETER["external_instance_for_reconcile_name"]

    # creates a GCP instance using the GCP API client library without going through idem
    create_external_instance(ctx, project, zone, name)

    resource_id = f"projects/{project}/zones/{zone}/instances/{name}"
    ret = await hub.exec.gcp.compute.instance.get(ctx, resource_id=resource_id)

    assert ret["result"], ret["comment"]
    assert ret["ret"]

    state_ctx = NamespaceDict({**ctx, "tag": "gcp.compute.instance_|test_|-tag"})

    res = await hub.states.gcp.compute.instance.absent(
        ctx=state_ctx, project=project, zone=zone, name=name
    )
    assert res["rerun_data"]

    while res["rerun_data"]:
        await asyncio.sleep(3)

        assert res["result"], res["comment"]
        assert res["old_state"]
        state_ctx["rerun_data"] = res["rerun_data"]
        res = await hub.states.gcp.compute.instance.absent(
            ctx=state_ctx, project=project, zone=zone, name=name
        )

    ret = await hub.exec.gcp.compute.instance.get(ctx, resource_id=resource_id)

    assert ret["result"]
    assert not ret["ret"]
    assert any("not found" in c for c in ret["comment"])


# TODO: Add all the created instances outside of fixtures in RESOURCES_TO_DELETE
#  Implemented only for the instance created in test_present
@pytest.fixture(autouse=True, scope="module")
async def test_cleanup(hub, ctx, idem_cli):
    yield

    # Do cleanup
    for resource_id, resource_data in RESOURCES_TO_DELETE.items():
        ret = hub.tool.utils.call_absent(
            idem_cli,
            resource_data["resource_type"],
            resource_data["name"],
            resource_id,
        )
        assert ret["result"], ret["comment"]
        assert not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def new_instance_with_multiple_disks(hub, idem_cli, tests_dir) -> Dict[str, Any]:
    path_to = f"{tests_dir}/sls/compute/instance_with_multiple_disks.sls"
    instance_resource_type = "compute.instance"
    with open(path_to) as template:
        data_template = template.read()
        present_ret = hub.tool.utils.call_present_from_sls(
            idem_cli, data_template.format(**PARAMETER)
        )
        assert present_ret["result"], present_ret["comment"]

        resource_id = present_ret["new_state"].get("resource_id")
        present_state_dict = yaml.safe_load(data_template.format(**PARAMETER))
        instance_name = list(present_state_dict.keys())[0]
        expected_state = {
            "resource_id": resource_id,
            "name": instance_name,
            **dict(
                ChainMap(
                    *present_state_dict[instance_name]["gcp.compute.instance.present"]
                )
            ),
        }

        # We need to add disks[0].source to the expected_state
        # because the disk for this VM was created with disks[0].initialize_params
        # which is a create-only property and the get method returns the newly created disk source property instead
        expected_state["disks"][0]["source"] = present_ret["new_state"]["disks"][0][
            "source"
        ]
        expected_state["disks"][1]["source"] = present_ret["new_state"]["disks"][1][
            "source"
        ]
        expected_state["id_"] = present_ret["new_state"]["id_"]
        expected_state["disks"][0].pop("initialize_params", None)
        expected_state["disks"][1].pop("initialize_params", None)
        changes = hub.tool.gcp.utils.compare_states(
            present_ret["new_state"],
            expected_state,
            instance_resource_type,
        )
        assert not bool(changes), changes

        yield present_ret["new_state"]

    absent_ret = hub.tool.utils.call_absent(
        idem_cli, instance_resource_type, instance_name, resource_id
    )
    assert absent_ret["result"], absent_ret["comment"]

    changes = hub.tool.gcp.utils.compare_states(
        absent_ret["old_state"],
        expected_state,
        instance_resource_type,
    )

    assert not bool(changes), changes
    assert not absent_ret.get("new_state")


@pytest.mark.asyncio
async def test_instance_result_does_not_match(
    hub, ctx, idem_cli, tests_dir, new_instance_with_multiple_disks
):
    assert new_instance_with_multiple_disks["disks"][1]["disk_size_gb"] == "20"
    new_instance_with_multiple_disks["disks"][1]["disk_size_gb"] = "30"
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, "compute.instance", new_instance_with_multiple_disks
    )

    assert ret["result"], ret["comment"]
    assert ret["new_state"], ret["comment"]
    assert ret["new_state"]["disks"][1]["disk_size_gb"] == "20"

    mismatch_comment = "Values mismatch between actual and desired states: {\"root['disks'][1]['disk_size_gb']\"}"
    assert mismatch_comment in ret["comment"]

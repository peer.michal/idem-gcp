from collections import ChainMap

import pytest

from idem_gcp.tool.gcp.utils import _is_within
from tests.utils import generate_unique_name

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": generate_unique_name("idem-test-health-check"),
    "project": "tango-gcp",
    "type_": "TCP",
    "port": 50000,
}
RESOURCE_TYPE = "compute.health_check"
RESOURCE_TYPE_FULL = "gcp.compute.health_check"
RESOURCE_ID = f"projects/{PARAMETER['project']}/global/healthChecks/{PARAMETER['name']}"

PRESENT_CREATE_STATE = {
    "name": PARAMETER["name"],
    "type_": PARAMETER["type_"],
    "tcp_health_check": {
        "port": PARAMETER["port"],
    },
}


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present_create")
def test_health_check_present_create(hub, idem_cli, tests_dir, __test, cleaner):
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE, PRESENT_CREATE_STATE, __test
    )

    assert ret["result"], ret["comment"]

    cleaner(ret["new_state"], "compute.health_check")

    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_create_comment(
                resource_type=RESOURCE_TYPE_FULL, name=PARAMETER["name"]
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.create_comment(
                resource_type=RESOURCE_TYPE_FULL, name=PARAMETER["name"]
            )
            in ret["comment"]
        )

    expected_state = {"resource_id": RESOURCE_ID, **PRESENT_CREATE_STATE}

    assert _is_within(ret["new_state"], expected_state, set())


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="present_update_read_only_field", depends=["present_create"]
)
def test_health_check_present_update_read_only_field(
    hub, idem_cli, tests_dir, __test, cleaner
):
    updated_name = "updated-name"
    update_state = {
        **PRESENT_CREATE_STATE,
        "resource_id": RESOURCE_ID,
        "name": updated_name,
    }
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE, update_state, __test
    )

    assert not ret["result"], ret["comment"]
    assert (
        hub.tool.gcp.comment_utils.non_updatable_properties_comment(
            RESOURCE_TYPE_FULL,
            updated_name,
            ("name",),
        )
        in ret["comment"]
    )

    assert (
        hub.tool.gcp.comment_utils.properties_mismatch_resource_id_comment(
            RESOURCE_TYPE_FULL, updated_name
        )
        in ret["comment"]
    )


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="present_update", depends=["present_update_read_only_field"]
)
def test_health_check_present_update(hub, idem_cli, tests_dir, __test, cleaner):
    update_state = {
        **PRESENT_CREATE_STATE,
        "resource_id": RESOURCE_ID,
        "check_interval_sec": 20,
        "timeout_sec": 5,
    }
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE, update_state, __test
    )

    assert ret["result"], ret["comment"]
    assert ret["new_state"], ret["comment"]

    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_update_comment(
                resource_type=RESOURCE_TYPE_FULL, name=PARAMETER["name"]
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.update_comment(
                resource_type=RESOURCE_TYPE_FULL, name=PARAMETER["name"]
            )
            in ret["comment"]
        )
    assert ret["new_state"].get("check_interval_sec") == 20
    assert ret["new_state"].get("timeout_sec") == 5


@pytest.mark.asyncio
async def test_describe(hub, ctx):
    ret = await hub.states.gcp.compute.health_check.describe(ctx)
    for resource_id in ret:
        assert "gcp.compute.health_check.present" in ret[resource_id]
        described_resource = ret[resource_id].get("gcp.compute.health_check.present")
        assert described_resource
        health_check = dict(ChainMap(*described_resource))
        assert health_check.get("resource_id") == resource_id


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="absent_with_invalid_resource_id", depends=["present_update"]
)
def test_absent_invalid_resource_id(hub, idem_cli, __test):
    invalid_resource_id = RESOURCE_ID + "-0"
    ret = hub.tool.utils.call_absent(
        idem_cli, RESOURCE_TYPE, None, invalid_resource_id, test=__test
    )

    # Considers the resource deleted.
    assert ret
    assert ret.get("comment")
    assert any("already absent" in c for c in ret["comment"])


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent", depends=["absent_with_invalid_resource_id"])
def test_absent(hub, idem_cli, __test):
    ret = hub.tool.utils.call_absent(
        idem_cli, RESOURCE_TYPE, None, RESOURCE_ID, test=__test
    )

    if __test:
        assert ret
        assert ret.get("comment")
        assert [
            hub.tool.gcp.comment_utils.would_delete_comment(
                resource_type=RESOURCE_TYPE_FULL, name=PARAMETER["name"]
            )
        ] == ret["comment"]
    else:
        # Considers the resource deleted.
        assert ret
        assert ret.get("comment")
        assert [
            hub.tool.gcp.comment_utils.delete_comment(
                resource_type=RESOURCE_TYPE_FULL, name=PARAMETER["name"]
            )
        ] == ret["comment"]

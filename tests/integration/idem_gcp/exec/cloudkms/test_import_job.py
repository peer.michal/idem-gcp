import pytest


@pytest.mark.asyncio
async def test_import_job_list(hub, ctx):
    parent = (
        f"projects/{ctx.acct.project_id}/locations/us-east1/keyRings/cicd-idem-gcp-1"
    )
    ret = await hub.exec.gcp.cloudkms.import_job.list(ctx, key_ring=parent)
    assert ret["result"], ret["comment"]
    assert len(ret["ret"]) > 0
    assert ret["ret"][0]["resource_id"] == f"{parent}/importJobs/cicd-import-job-1"


@pytest.mark.asyncio
async def test_import_job_get(hub, ctx):
    resource_id = f"projects/{ctx.acct.project_id}/locations/us-east1/keyRings/cicd-idem-gcp-1/importJobs/cicd-import-job-1"
    ret = await hub.exec.gcp.cloudkms.import_job.get(ctx, resource_id=resource_id)
    assert ret["result"], ret["comment"]
    assert ret["ret"]["resource_id"] == resource_id


@pytest.mark.asyncio
async def test_import_job_get_empty(hub, ctx):
    resource_id = f"projects/{ctx.acct.project_id}/locations/us-east1/keyRings/cicd-idem-gcp-1/importJobs/cicd-import-job-100"
    ret = await hub.exec.gcp.cloudkms.import_job.get(ctx, resource_id=resource_id)
    assert ret["result"], ret["comment"]
    assert len(ret["comment"]) == 1 and ret["comment"][
        0
    ] == hub.tool.gcp.comment_utils.get_empty_comment(
        "gcp.cloudkms.import_job", resource_id
    ), ret[
        "comment"
    ]

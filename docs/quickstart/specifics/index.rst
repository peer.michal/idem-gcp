===============
Usage specifics
===============

This page aims to provide more detailed information about using the GCP plugin to achieve complex scenarios.

.. grid:: 1

    .. grid-item-card:: Controlling resource discovery
        :link: discovery
        :link-type: doc

        Find out how to get more fine-grained control over creating and updating GCP resources.

    .. grid-item-card:: SLS properties documentation
        :link: apidocs
        :link-type: doc

        Find out where to look for documentation and examples when creating your SLS files.

    .. grid-item-card:: Executing long-running operations
        :link: longrunning
        :link-type: doc

        Learn how long-running operations work in the GCP plugin.



.. toctree::
   :maxdepth: 3
   :hidden:

   discovery
   apidocs
   longrunning

"""Metadata module for managing ServiceAccountKeys."""

PATH = "projects/{project}/serviceAccounts/{serviceAccount}/keys"

NATIVE_RESOURCE_TYPE = "iam.projects.service_accounts.keys"

.. note::
    A prerequisite for running the example commands is
    to have your GCP credentials set up using the ACCT_KEY and ACCT_FILE environment variables.
    For more information, refer to :ref:`configure`
